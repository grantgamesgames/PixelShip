PixelShip
=========

[astrism.github.com/PixelShip](http://astrism.github.com/PixelShip)

Simple Battleship game created as an example for some internship interviewees.

Limitations/Simplifications
* meant to be programmed in 30mins, took me ~1 hour
* Attacks from computer are random
* Ships are 1x1
* Could use some more UI for feedback
* Did it in JSFiddle, would have been a lot faster in a real text editor, decided not to ask interviewees to use JSFiddle, going to use skype instead :)